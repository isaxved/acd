package kz.aitu.week1.homework;

import java.util.Scanner;

public class ReverseArray {

    static void reversePrint(int[] array,int a){
        if(a == array.length){
            System.out.println(array[a]);
            return;
        }
        reversePrint(array,a++);
        System.out.println(array[a]);

    }

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int len = input.nextInt();
        int[] a = new int[len];
        for(int i = 0; i < len;i++){
            a[i] = input.nextInt();
        }

        reversePrint(a,0);
    }
}

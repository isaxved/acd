package kz.aitu.week1.Quiz1;

import java.util.Scanner;

public class Palindrome {

    public static boolean findPalindrome(String someString,int begin,int end){
        boolean check = false;
        if(begin+1 == someString.length()){
           check = true;
           return check;
       }
        else if(someString.charAt(begin)==someString.charAt(end)){
            check = true;
            findPalindrome(someString,begin+1,end-1);
        }
        return check;
    }

    public static void main(String[] args){
        Scanner cin = new Scanner(System.in);
        String someString = cin.nextLine();
        System.out.println((findPalindrome(someString,0,someString.length()-1)));
    }
}

package kz.aitu.week1.Quiz1;
import java.sql.SQLOutput;
import java.util.Scanner;

public class QuizTask1 {

    static void returnSequence(int a,int b){
        if(b <= a) {
            System.out.println(b);
            b++;
            returnSequence(a,b);
        }
    }

    public static void main(String[] args){
        int userInput;
        int counter = 1;
        System.out.println("Enter number: ");
        Scanner cin = new Scanner(System.in);
        userInput = cin.nextInt();
        returnSequence(userInput,counter);
    }
}

package kz.aitu.cs1902quiz;

public class Main {
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        tree.addNode(3);
        tree.addNode(4);
        tree.addNode(5);
        tree.addNode(6);
        tree.addNode(2);
        tree.addNode(1);
        tree.addNode(31);

        int x = tree.TotalSum(tree.getRoot());
        System.out.println(x);
        int y = tree.getHeight(tree.getRoot());
        System.out.println(y);
        int z = tree.findMax(tree.getRoot());
        System.out.println(z);
        // for last occurence BinarySearch
        int[] array = new int[9];
        for(int i = 0; i < 9; i++){
            if (i == 0 || i == 8) {
                array[i] = 6;
            }
            array[i] = 2;
        }
        int some = tree.lastOccurence(array,2);
        System.out.println(some);
    }
}

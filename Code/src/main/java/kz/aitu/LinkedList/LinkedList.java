package kz.aitu.LinkedList;

public class LinkedList {
    private Node head;
    private Node tail;

    public void addNode (int data) {
        Node temp = new Node(data);

        if(head == null){
            head = temp;
            tail = temp;
        } else {
            tail.setNext(temp);
            tail = temp;
        }
    }

    public void insertAtPoint (Node head, int index,int data) {
        Node node = new Node(data);
        if (index == 0) {
            node.setNext(head);
            head = node;
        } else {
            Node temp = head;
            for(int i = 0; i < index-1; i++) {
                temp = temp.getNext();
            }
            node.setNext(temp.getNext());
            temp.setNext(node);
        }
    }

    public void deleteAtPoint (Node head, int index) {

        if(index == 0) head = head.getNext();
        else {
            Node temp = head;

            for (int i = 0; i < index - 1; i++) {
                temp = temp.getNext();
            }
            temp.setNext(temp.getNext().getNext());
        }
        return;
    }

    public boolean compareLists (Node one, Node two) {
        if ( one == null && two == null) return  true;
        if ( one != null && two == null) return  false;
        if ( one == null && two != null) return  false;
        return compareLists(one.getNext(),two.getNext());
    }

    public void deleteDuplicate (Node head) {
        Node temp = head;
        if(temp == null || temp.getNext() == null) return;
        if(temp.getData() == temp.getNext().getData()){
            if(temp.getNext().getNext() != null) {
                temp.setNext(temp.getNext().getNext());
                deleteDuplicate(temp);
            }
            else
                temp.setNext(null);
            return;
        }
        if(temp.getData() != temp.getNext().getData() && temp.getNext() != null) deleteDuplicate(temp.getNext());
    }

    public void print() {
        Node temp = head;
        while (temp != null){
            System.out.println(temp.getData());
            temp = temp.getNext();
        }
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }
}

package kz.aitu.MidtermRedo;

public class Main {
    public static void main(String[] args) {

        Stack stack1 = new Stack();
        Stack stack2 = new Stack();
        Stack stack3 = new Stack();

        stack1.push(9);
        stack1.push(8);
        stack1.push(7);
        stack1.push(6);

        stack2.push(21);
        stack2.push(23);
        stack2.push(41);
        stack2.push(13);

        stack3.push(113);
        stack3.push(133);
        stack3.push(143);
        stack3.push(153);

        System.out.println(stack1.conjugate(stack1.getTop(),stack2.getTop(),stack3.getTop()));
    }


}

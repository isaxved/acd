package kz.aitu.Week6.OlkiTree;

public class BSTree {
    private Node root;

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public BSTree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node node = new Node(key, value);
        if(root == null) root = node;
        else {
            traverse(root,node);
        }
    }

    public void traverse (Node root, Node node) {
        if(root == null) {
            root = node;
            return;
        }
        if(root.getKey() < node.getKey()){
            if(root.getRight() == null) root.setRight(node);
            else
                traverse(root.getRight(),node);
        }
        if(root.getKey() > node.getKey()){
            if(root.getLeft()==null) root.setLeft(node);
            else
                traverse(root.getLeft(),node);
        }
    }

    public boolean delete (int key) {
        if(root == null) return false;
        if(root.getLeft() == null){
            root = root.getRight();
            return true;
        }
        if(root.getRight() == null){
            root = root.getLeft();
            return true;
        }
        if(root.getKey() == key) {
           Node newNode = deleteRoot(root);
           newNode.setLeft(root.getLeft());
           newNode.setRight(root.getRight());
           root = newNode;
           return true;
        }
        else{
            deleteNode(root,key);
        }
        return false;
    }



    private void deleteNode(Node node,int key){
        Node tem1 = node;
        Node tmp = findNodeV2(tem1,key);

        if(tmp.getLeft() == null) {
            tmp = tmp.getRight();
            return;
        }
        if(tmp.getRight()==null){
            tmp = tmp.getLeft();
            return;
        }
        else
            tmp = null;
    }

    private Node findNodeV2 (Node node, int key){
        if(node == null) return null;
        if(node.getKey() == key) return node;
        if(node.getLeft() != null) return findNodeV2(node.getLeft(),key);
        if(node.getRight() != null) return findNodeV2(node.getRight(),key);
        return null;
    }

    private Node deleteRoot(Node node) {
        Node temp = node.getLeft();
        while(temp.getRight() != null){
            temp = temp.getRight();
        }
        if(temp.getLeft() != null) {
            Node nodeForReplace = new Node(temp.getKey(),temp.getValue());
            Node s1 = node.getLeft();
            while (s1 != temp){
                s1 = s1.getRight();
            }
            s1.setRight(temp.getLeft());
            return nodeForReplace;
        } else {
            Node tmp = new Node(temp.getKey(),temp.getValue());
            temp = null;
            return tmp;
        }
    }


    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending() {
        printAsc(root);
        System.out.println();
    }

    private void printAsc (Node node) {
        if(node == null) return;
        printAsc(node.getLeft());
        System.out.print(node.getValue()+ " ");
        printAsc(node.getRight());
    }

    public void printAll() {
        String[][] vaultLeft = new String[100][100];
        String[][] vaultRight = new String[100][100];
        String[][] vaultFinal = new String[100][100];
        vaultFinal[0][0] = root.getValue();
        ChildSalvation(0,0,vaultLeft,root.getLeft());
        ChildSalvation(0,0,vaultRight,root.getRight());

        for(int i = 1; i < 10; i++){
            for(int j = 0; j < 10; j++) {
                if (vaultLeft[i-1][j] != null){
                    System.out.print(vaultLeft[i-1][j] + " ");
                    vaultFinal[i][j] = vaultLeft[i-1][j];
                }
                System.out.println();
            }
        }
        for(int i = 1; i < 10; i++){
            for(int j = 2; j < 10; j++) {
                if (vaultRight[i-1][j-2] != null){
                    vaultFinal[i][j] = vaultRight[i-1][j-2];
                }
            }
        }

        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++) {
                if (vaultFinal[i][j] != null){
                    System.out.print(vaultFinal[i][j] + " ");
                }
            }
        }
    }

    private void ChildSalvation (int x, int y, String[][] storage,Node node) {
        if(node == null) return;
        storage[x][y] = node.getValue();
        ChildSalvation(x+1,y,storage,node.getLeft());
        ChildSalvation(x+1,y+1,storage,node.getRight());
    }



}

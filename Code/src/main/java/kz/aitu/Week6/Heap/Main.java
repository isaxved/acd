package kz.aitu.Week6.Heap;

public class Main {

    public static void main(String[] args) {
        HeapTreeMin heap = new HeapTreeMin();

        heap.insertNode(10);
        heap.insertNode(2);
        heap.insertNode(1);
        heap.insertNode(13);
        heap.insertNode(12);
        heap.insertNode(23);
        heap.insertNode(14);

        System.out.println(heap.getRoot().getLeft().getData());

    }
}

package kz.aitu.gradeUp3;

public class Queue {
    private Node head;

    public void push (int data) {
        Node node = new Node(data);
        if(head == null) head = node;
        else {
            node.setNext(head);
            head = node;
        }
    }

    public int pop () {
        int temp = head.getData();
        head = head.getNext();
        return temp;
    }

    public int peek () {
        if(head == null) return 0;
        return head.getData();
    }

    public int size () {
        int counter = 0;
        Node t = head;
        while (t != null) {
            counter++;
            t = t.getNext();
        }
        return counter;
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }
}

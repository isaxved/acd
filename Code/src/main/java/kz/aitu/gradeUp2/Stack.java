package kz.aitu.gradeUp2;

public class Stack {
    private Node top;

    public void push (int data) {
        Node temp = new Node(data);
        if(top == null) {
            top = temp;
        } else {
            temp.setNext(top);
            top = temp;
        }
    }

    public boolean empty () {
        return top == null;
    }

    public int size () {
        int counter = 0;
        Node t = top;
        while ( t != null) {
            t = t.getNext();
            counter++;
        }
        return counter;
    }

    public int pop () {
        Node temp = top;
        top = temp.getNext();
        return temp.getData();
    }

    public void print() {
        Node n = top;
        while (n != null) {
            System.out.print(n.getData() + " ");
            n = n.getNext();
        }
        System.out.println();
    }

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }
}

package kz.aitu.week5Midterm2;
// big O (n^2)
public class Stack {
    Node topStack;
    private int top = 0;
    int array[] = new int[1000];

    public void push(int data){
        array[top] = data;
        top++;
    }
    public void pop() {
        array[top] = 0;
        top--;
    }

    public void sortStack(Node node){
        for(int i = 0; i < top-1;i++) {
            for (int j = 0; j < top - 2; j++) {
                if (array[top] > array[top + 1]) {
                    int temp = array[top];
                    array[top] = array[top + 1];
                    array[top + 1] = temp;
                }
            }
        }
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
    // Big O(N^2)
    public void compare(int[] node, int nodeSize, int[] node2, int nodeSize2){
        int[] arrayNew = new int[100];
        int counter = 0;
        for(int i = 0;i < nodeSize; i++){
            for(int j = 0; j < nodeSize2; j++){
                if(node[i] == node2[j]) continue;
                else {
                    arrayNew[counter] = node[i];
                    arrayNew[counter++] = node[j];
                    counter++;
                }
            }
        }
        for(int i : arrayNew){
            System.out.println(arrayNew);
        }
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }
   /* private Node top;

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }

    public void push(int value){
        Node newNode = new Node(value);
        if (top == null){
            top = newNode;
        } else {
            newNode.setNext(top);
            top = newNode;
        }
    }

    public void pop(){
        top = top.getNext();
    }

    public void print(Node node){
        Node temp = top;
        while(temp != null) {
            System.out.println(temp.getData());
            temp = temp.getNext();
        }
    }
*/
}

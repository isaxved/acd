package kz.aitu.week5Midterm1;
//Big O(N)
public class LinkedList {
    private Node head;
    private Node tail;


    public void printRecursively (Node node) {
        if(node == null) return;
        if(node != null){
            System.out.println(node.getData());
            node = node.getNext();
            printRecursively(node);
        }
    }

    public void push_back (int data) {
        Node temp = new Node(data);
        tail.setNext(temp);
        tail = temp;
    }

    public void add_node(int data) {
        Node temp = new Node(data);
        if(head == null) {
            head = temp;
            tail = temp;
        } else {
            tail.setNext(temp);
            tail = temp;
        }
    }


    public void push_back() {

    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }
}

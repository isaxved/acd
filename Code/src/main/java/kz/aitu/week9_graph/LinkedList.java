package kz.aitu.week9_graph;

public class LinkedList {
    private Vertex head;
    private Vertex tail;

    public LinkedList() {
        this.head = null;
        this.tail = null;
    }

    public void addEdge(Vertex vertex) {
        if (head == null) {
            head = vertex;
            tail = vertex;
        } else {
            tail.setNext(vertex);
            tail = vertex;
        }
    }

    public boolean contain(Vertex vertex) {
        if (head == null) return false;
        Vertex temp = head;
        while (temp != null) {
            if (temp.getKey() == vertex.getKey()) return true;
            temp = temp.getNextforlist();
        }
        return false;
    }


    public Vertex getHead() {
        return head;
    }

    public void setHead(Vertex head) {
        this.head = head;
    }

    public Vertex getTail() {
        return tail;
    }

    public void setTail(Vertex tail) {
        this.tail = tail;
    }
}

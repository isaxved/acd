package kz.aitu.Week8;

public class Selection {
    private int[] array = new int[100];
    private int current_minimum;
    private int array_item;


    public void sort(int[] array) {

        for (int i = 0 ; i < array.length; i++){
            current_minimum = array[i];
            for(int j = i; j < array.length; j++) {
                if (current_minimum > array[j]){
                    current_minimum = array[j];
                    array_item = j;
                }
            }
            int temp = array[i];
            array[i] = current_minimum;
            array[array_item] = temp;
        }
    }
}

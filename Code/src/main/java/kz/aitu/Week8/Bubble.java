package kz.aitu.Week8;

public class Bubble {

    private int[] someArray;

    public void Sort(int[] someArray) {
        for (int i = 0; i < someArray.length; i++){
            for (int j = 0; j < someArray.length - 1; j++){
                if(someArray[j] > someArray[j+1]) {
                    int temp = someArray[j];
                    someArray[j] = someArray[j+1];
                    someArray[j+1] = temp;
                }
            }
        }
    }
}

package kz.aitu.Week8;

public class Quick {

    public void sort(int[] array){
        QuickSort(array,0,array.length-1);
    }

    private void QuickSort(int[] array, int left, int right) {
        if (left < right) {

            int index = partition(array,left,right);

            QuickSort(array,left,index-1);
            QuickSort(array,index+1,right);

        }
    }

    private int partition(int[] array, int left, int right) {
        int index = array[right];
        int j = left-1;
        for (int i = left; i < right; i++) {
            if (array[i] < index) {
                j++;
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        int temp = array[j+1];
        array[j+1] = array[right];
        array[right] = temp;
        return j+1;
    }

}

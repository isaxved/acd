package kz.aitu.Week9aa;

public class Queue {
    private Node head;
    private Node tail;
    private int size = 0;

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void add(BinaryNode node) {
        size++;
        Node temp = new Node(node);
        if (head == null) {
            setHead(temp);
            setTail(temp);
        } else {
            tail.setNext(temp);
            tail = temp;
        }
    }

    public BinaryNode poll() {
        if (head == null) return null;
        BinaryNode temp = head.getNode();
        head = head.getNext();
        return temp;
    }

    public boolean isEmpty() {
        return head == null;
    }
    public void pop() {
        if (head == null) {
            return;
        } else {
            head = head.getNext();
        }
    }

}

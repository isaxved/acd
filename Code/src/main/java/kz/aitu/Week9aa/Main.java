package kz.aitu.Week9aa;

import com.sun.tools.jdeprscan.scan.Scan;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        BinaryTree tree = new BinaryTree();
        tree.insert(7);
        tree.insert(10);
        tree.insert(9);
        tree.insert(3);
        tree.insert(4);
        tree.insert(12);
        tree.insert(11);
        tree.insert(1);
        tree.insert(18);
        tree.insert(6);
        tree.insert(4);
        tree.PrintTree(tree.getRoot());
        System.out.println(tree.DFS(tree.getRoot(),6));
        System.out.println(tree.BFS(tree.getRoot(),6));
    }
}

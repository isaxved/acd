package kz.aitu.Week9aa;


public class Stack {
    Node top;

    public void push (BinaryNode node) {
       Node temp = new Node(node);
        if(top == null) top = temp;
        else {
            temp.setNext(top);
            top = temp;
        }
    }

    public BinaryNode poll () {
        if (top == null) return null;
        BinaryNode temp = top.getNode();
        top = top.getNext();
        return temp;
    }

    public void pop () {
        if(top == null) return;
        top = top.getNext();
    }

    public boolean isEmpty() {
        return top == null;
    }
}

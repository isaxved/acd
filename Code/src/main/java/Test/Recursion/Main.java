package Test.Recursion;

public class Main {
    public static void main(String[] args) {
        int[] myIntArray = new int[]{1,2,3,4,5};
        RecursionSumOfArray some = new RecursionSumOfArray();

        System.out.println( some.SumOfArrayItems(myIntArray,0));
        System.out.println(some.CountItemsInArray(myIntArray,0));
        System.out.println(some.FindBiggestInt(myIntArray,0));
    }
}

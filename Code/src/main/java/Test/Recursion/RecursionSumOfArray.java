package Test.Recursion;

public class RecursionSumOfArray {

    public int FindBiggestInt (int[] array, int some) {
        if (some == array.length) {
            return 0;
        }
        return Math.max(array[some], FindBiggestInt(array,some+1));
    }

    public int CountItemsInArray (int[] array, int some) {
        if (some == array.length) {
            return 0;
        }
        return CountItemsInArray(array, some + 1) + 1;
    }

    public int SumOfArrayItems (int[] someArray, int some) {
        if (some == someArray.length) {
            return 0;
        }

            return SumOfArrayItems(someArray, some+1) + someArray[some];

    }
}
